({
	doInit : function(component, event, helper) {
        var opts = [];
        opts = [ 
            { label: "Active", value: "Active", selected: "true"},
            { label: "Inactive", value: "Inactive" },
            { label: "All", value: "All"},
        ];  
            component.set("v.filterOptions", opts); 
        /*var action = component.get("c.objectNames");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {           
                var allValues = response.getReturnValue();
                component.set("v.pickl", allValues);
            }        	         
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } 
                else {
                    console.log("Unknown Error");
                }
            }
        });
        $A.enqueueAction(action);*/
	},
    
    doSearch : function(component, event, helper) {
        debugger;
        var pickselected = component.find("selectid").get("v.value");
        console.log('pickselected--->' + pickselected);
		component.set('v.selectedValue', pickselected);
        var selected = component.get('v.selectedValue');
        console.log('Selected--->' + selected);
        var action = component.get("c.objectRecords");
        action.setParams({selectedObject : "Account"});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {  
                //component.find('sfdcDiv').set("v.body",[]);
                var allValues = response.getReturnValue();
            	console.log('allValues--->' + allValues);
                var objectValue = allValues.sObjectDatastr;
                console.log('objectValue--->' + objectValue);
                var fieldList = allValues.fieldList;
                console.log('fieldList--->' + fieldList);
                
                /* Create Dynamic Table */
                var sObjectDataTableHeader = [];
                // Create table Header
                for (var i=0; i<fieldList.length; i++) {
                	sObjectDataTableHeader.push(fieldList[i].label);
                }
                console.log('sObjectDataTableHeader--->>' + sObjectDataTableHeader);
                //Get the count of columns.
                var columnCount = sObjectDataTableHeader.length;
                //Create a HTML Table element.
                var table = document.createElement("TABLE");
                //table.border='2';
                //Add the header row.
                var row = table.insertRow(-1);
                for (var i=0; i<columnCount; i++) {
                	var headerCell = document.createElement("TH");
                    //headerCell.width='75';
                    headerCell.innerHTML = sObjectDataTableHeader[i];
                    headerCell.className='headerClass';
                    row.appendChild(headerCell);
                }
                var dvTable = document.getElementById("sfdctable");
                dvTable.innerHTML = "";
                dvTable.appendChild(table);
                debugger;
                /* Create Dynamic Table End */    
                if(objectValue.length){
                	for(var j=0; j<objectValue.length; j++){
                    	// Dynamic table Row
                        row = table.insertRow(-1);
                        // Dynamic Table Row End
                        for (var i=0; i<fieldList.length; i++) {
                            // Dynamic table Row
                            if(objectValue[j].split("|")[i] == undefined){
                                var cell = row.insertCell(-1);   
                                cell.innerHTML = null;
                            }
                            else if(fieldList[i].label == objectValue[j].split("|")[i].split(':')[0]){
                                var cell = row.insertCell(-1);                           
                                cell.innerHTML = objectValue[j].split("|")[i].split(':')[1];
                            }else{
                                var cell = row.insertCell(-1);   
                                cell.innerHTML = null;
                            }                      
                        }
                    }
                }
            }        	         
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } 
                else {
                    console.log("Unknown Error");
                }
            }
        });
        $A.enqueueAction(action);
    }
})