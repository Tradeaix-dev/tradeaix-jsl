({
	 scriptsLoaded : function(component, event, helper) {
        console.log('Script loaded..'); 
    },
    getReportList : function(component, event, helper) {
        debugger;
        var Action = component.get("c.getcp");
        component.set("v.userid",localStorage.getItem("UserSession"));
        var sessionname = localStorage.getItem("UserSession");       
        Action.setCallback(this, function(actionResult) {
            debugger;
            var state = actionResult.getState(); 
            var results = actionResult.getReturnValue();
            if(state === "SUCCESS"){                
                component.set("v.Report",results.getcp); 
                // when response successfully return from server then apply jQuery dataTable after 500 milisecond
                setTimeout(function(){ 
                    $('#tableId').DataTable();
                    // add lightning class to search filter field with some bottom margin..  
                    $('div.dataTables_filter input').addClass('slds-input');
                    $('div.dataTables_filter input').css("marginBottom", "10px");
                }, 500);
            }
        });
        $A.enqueueAction(Action);
        
	}
})