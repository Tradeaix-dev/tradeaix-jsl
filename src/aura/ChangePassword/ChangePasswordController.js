({
	updatePassword : function(component, event, helper) {
        debugger;
		var oldPassword = component.find("oldPassword").get("v.value");
        var newPassword = component.find("newPassword").get("v.value");
        var confirmPassword = component.find("confirmPassword").get("v.value");  
        if(newPassword != confirmPassword){
            component.set("v.validation",true);
        }else{
            var sessionname = localStorage.getItem("UserSession");
            var Action1 = component.get("c.changePassword");
            
            Action1.setParams({
                "username" : sessionname,
                "oldpassword": oldPassword,
                "newpassword": newPassword
            });
            Action1.setCallback(this, function(actionResult1) {
                debugger;
                var state = actionResult1.getState(); 
                var results = actionResult1.getReturnValue();
                if(state === "SUCCESS"){
                    var site = $A.get("{!$Label.c.Org_URL}");
                    window.location = '/'+site+'/s/dashboard';
                }
            });
            $A.enqueueAction(Action1);
        }
    }
})