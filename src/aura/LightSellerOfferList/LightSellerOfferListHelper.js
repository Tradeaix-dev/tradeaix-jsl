({
    convertArrayOfObjectsToXcel : function(component, objectRecords){ 
        // declare variables
        var csvStringResult, counter, keys, keys1,keys2,columnDivider, lineDivider;
        
        // check if "objectRecords" parameter is null, then return from function
        if (objectRecords == null || !objectRecords.length) {
            return null;
        }
        // store ,[comma] in columnDivider variabel for sparate CSV values and 
        // for start next line use '\n' [new line] in lineDivider varaible  
        columnDivider = ',';
        lineDivider =  '\n';
        debugger;
        // in the keys valirable store fields API Names as a key 
        // this labels use in CSV file header 
        keys2 = ['Market Place Export Results',''];  
        keys1 = ['Offer ID','Offer Status','CU Name','Offer Name','Published Date','Current Principal $','Portfolio Price $','Servicing Fee (bps)','Indirect %','WAC%','WA LTV %','WA FICO','WA Remaining Term (months)' ];
        keys = ['OfferId__c','Offer_Status__c','CU_Name__c','Name','PublishedDate__c','OutstandingPrincipal__c','PortfolioPrice__c','ServicingFee__c','IndirectPercentage__c','WAC__c','LoanToValue__c','FICO_WA__c','RemainingTerm__c' ];
       
        csvStringResult = '';
        csvStringResult += keys2.join(lineDivider);
        csvStringResult += keys1.join(columnDivider);
        csvStringResult += lineDivider;
        
        for(var i=0; i < objectRecords.length; i++){   
            counter = 0; 
            for(var sTempkey in keys) {
                var skey = keys[sTempkey] ;  
                // add , [comma] after every String value,. [except first]
                if(counter > 0){ 
                    csvStringResult += columnDivider; 
                }
               
                    
                if(skey == 'CU_Name__c' && objectRecords[i].CreditUnion[skey] != undefined) 
                    csvStringResult += '"'+ objectRecords[i].CreditUnion[skey]+'"'; 
                else if(skey =='OutstandingPrincipal__c') 
                    csvStringResult += '"'+ '$'+ objectRecords[i].Offer[skey]+'"';  
                 else if(objectRecords[i].Offer[skey] != undefined) 
                    csvStringResult += '"'+ objectRecords[i].Offer[skey]+'"';     
                counter++; 
            } // inner for loop close 
            csvStringResult += lineDivider;
        }// outer main for loop close  
        // return the CSV formate String 
        return csvStringResult;        
    },
    convertArrayOfObjectsToXcel : function(component, objectRecords, userType){ 
        // declare variables
        var csvStringResult, counter, keys, keys1,keys2,columnDivider, lineDivider;
        
        // check if "objectRecords" parameter is null, then return from function
        if (objectRecords == null || !objectRecords.length) {
            return null;
        }
        // store ,[comma] in columnDivider variabel for sparate CSV values and 
        // for start next line use '\n' [new line] in lineDivider varaible  
        columnDivider = ',';
        lineDivider =  '\n';
		
        debugger;      
        
        // in the keys valirable store fields API Names as a key 
        // this labels use in CSV file header 
        keys2 = ['Market Place Export Results',''];  
        if(userType == 2){
            keys2 = ['Seller Offers Export Results',''];  
        }
        keys1 = ['Offer ID','Offer Status','CU Name','Offer Name','Published Date','Current Principal $','Portfolio Price $','Servicing Fee (bps)','Indirect %','WAC%','WA LTV %','WA FICO','WA Remaining Term (months)' ];
        keys = ['OfferId__c','Offer_Status__c','CU_Name__c','Name','PublishedDate__c','OutstandingPrincipal__c','PortfolioPrice__c','ServicingFee__c','IndirectPercentage__c','WAC__c','LoanToValue__c','FICO_WA__c','RemainingTerm__c' ];
       
        csvStringResult = '';
        csvStringResult += keys2.join(lineDivider);
        csvStringResult += keys1.join(columnDivider);
        csvStringResult += lineDivider;
        
        for(var i=0; i < objectRecords.length; i++){   
            counter = 0; 
            for(var sTempkey in keys) {
                var skey = keys[sTempkey] ;  
                // add , [comma] after every String value,. [except first]
                if(counter > 0){ 
                    csvStringResult += columnDivider; 
                }
               
                    
                if(skey == 'CU_Name__c' && objectRecords[i].CreditUnion[skey] != undefined) 
                    csvStringResult += '"'+ objectRecords[i].CreditUnion[skey]+'"'; 
                else if(skey =='OutstandingPrincipal__c') 
                    csvStringResult += '"'+ '$'+ objectRecords[i].Offer[skey]+'"';  
                 else if(objectRecords[i].Offer[skey] != undefined) 
                    csvStringResult += '"'+ objectRecords[i].Offer[skey]+'"';     
                counter++; 
            } // inner for loop close 
            csvStringResult += lineDivider;
        }// outer main for loop close  
        // return the CSV formate String 
        return csvStringResult;        
    },
    numberWithCommas: function(x) {  
        x =String(x).toString();
        var afterPoint = '';
        if(x.indexOf('.') > 0)
            afterPoint = x.substring(x.indexOf('.'),x.length);
        x = Math.floor(x);
        x=x.toString();
        var lastThree = x.substring(x.length-3);
        var otherNumbers = x.substring(0,x.length-3);
        if(otherNumbers != '')
            lastThree = ',' + lastThree; 
        return otherNumbers.replace(/\B(?=(\d{3})+(?!\d))/g, ",") + lastThree + afterPoint;
    },    
    checkSpecialCharecter : function(data){ 
        var iChars = "!`@#$%^&*()+=-[]\\\';/{}|\":<>?~_";   
        for (var i = 0; i < data.length; i++)
        {      
            if (iChars.indexOf(data.charAt(i)) != -1)
            {    
                return true;
            } 
        }
        return false;
    },
    checkSpecialCharecter1 : function(data){ 
        var iChars = "!`@#$%^&*()+=-[]\\\';.,/{}|\":<>?~_";   
        for (var i = 0; i < data.length; i++)
        {      
            if (iChars.indexOf(data.charAt(i)) != -1)
            {    
                return true;
            } 
        }
        return false;
    },
    checkCharecter : function(data){ 
        for (var i = 0; i < data.length; i++)
        {   
            if (data.match(/[^0-9\.,]/g, ''))
            {    
                return true;
            } 
        }
        return false;
    },     
    numberWithoutCommas : function(data){
        var result = '';
        for (var i = 0; i < data.toString().split(',').length; i++) { 
            result += data.toString().split(',')[i];
        } 
        return result;
    },     
    
    round : function(num, place) { 
        var div = '1';
        for (var i = 0; i < place; i++) { 
            div = div + '0' ;
        }
        var divideby = Number(div);
        return (Math.round(num +'e+' + place)/divideby).toFixed(place); 
        //return +(Math.round(number + "e+" + place)  + "e-" + place);
    },
})