@RestResource(urlMapping='/UserManagementCheck/*') 

global with sharing class LoginAPIcall {
    @HttpGet
    global static  String checkUserManagement() {
        system.debug('<<8<<');
        string ReturnRes='';
        RestRequest req = RestContext.request;
        system.debug('<<9<<'+req);
        RestResponse res = RestContext.response; 
        system.debug('<<10<<'+res);
        String requestStr = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
        system.debug('<<11<<'+requestStr);
        List<User_Management__c> result;
        if(requestStr != null && requestStr != '' && requestStr.contains('&')){
            result = [Select Name,id,Password__c, User_Name__c From User_Management__c WHERE User_Name__c= :requestStr.split('&')[0] AND Password__c =: requestStr.split('&')[1]];
            system.debug('<<12<<'+result);
            if(result.size() > 0)
                ReturnRes = result[0].Name;
            else 
                ReturnRes = 'No Record';
        }else{
            ReturnRes = 'No Record';
        }
        return ReturnRes;
    }
    
}