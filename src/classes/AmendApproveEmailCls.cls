public class AmendApproveEmailCls {
    public Id amendId {get;set;} 
    public class amendDetails { 
        public String comments{get;set;}   
        public String Thanksmsg{get;set;}
        public String logo{get;set;}
        public String logotitle{get;set;} 
        public String TransId{get;set;}
        public String tenantname{get;set;} 
    }
    public amendDetails email {get;set;}  
    public amendDetails getAmends()
    {
        email = New amendDetails();
        Amend_Attributes__c AA = [SELECT Id, Name, Comments__c, Amend_Status__c, Amend__c, Transaction__c, Transaction__r.CreatedBy__c from Amend_Attributes__c WHERE Id =: amendId];
        email.comments = AA.Comments__c;
        email.TransId = AA.Transaction__c;
        
        if(AA.Amend_Status__c == 'Submitted for Approval'){
            Quotes__c lstQ = [SELECT Id, Name, CreatedBy__c from Quotes__c WHERE Transaction__c =: AA.Transaction__c AND Bid_Status__c = 'Quote Closed'];
            User_Management__c partyUser = [SELECT Id, Tenant__c,Tenant__r.Tenant_Site_Name__c ,Tenant__r.Tenant_Short_Name__c,Name,First_Name__c,Last_Name__c,User_Name__c,Profiles__r.Name,User_Email__c,Street__c, City__c,State_Province__c,Title__c,Phone__c,Tenant__r.Tenant_Footer_Message__c, Tenant__r.Tenant_Logo_Url__c from User_Management__c WHERE Id =: lstQ.CreatedBy__c];
            email.Thanksmsg = partyUser.Tenant__r.Tenant_Footer_Message__c;
            email.logo = partyUser.Tenant__r.Tenant_Logo_Url__c;
            email.tenantname = partyUser.Tenant__r.Tenant_Short_Name__c;
        }else{
            User_Management__c partyUser = [SELECT Id, Tenant__c,Tenant__r.Tenant_Site_Name__c ,Tenant__r.Tenant_Short_Name__c,Name,First_Name__c,Last_Name__c,User_Name__c,Profiles__r.Name,User_Email__c,Street__c, City__c,State_Province__c,Title__c,Phone__c,Tenant__r.Tenant_Footer_Message__c, Tenant__r.Tenant_Logo_Url__c from User_Management__c WHERE Id =: AA.Transaction__r.CreatedBy__c];
            email.Thanksmsg = partyUser.Tenant__r.Tenant_Footer_Message__c;
            email.logo = partyUser.Tenant__r.Tenant_Logo_Url__c;
            email.tenantname = partyUser.Tenant__r.Tenant_Short_Name__c;
        }
        return email;
        
    }
}