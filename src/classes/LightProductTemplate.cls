public with sharing class LightProductTemplate {
    public static String query = ''; 
    public static Integer size = 0; 
     @AuraEnabled 
    public static Organization__c[] loadorgs(String username) {
        return Helper.loadorgs(username);
    }
    @AuraEnabled
    public static String getloggeduserBrowserTitle() { 
        String Btitle = '';
    
        return Btitle;
    }
    @AuraEnabled
    public static LightProductTemplateListV Filter(Integer counter, String sortbyField, String sortDirection, String selected,String userid,String Filtervalue) { 
        //Helper.setSessionValue('sessionselected', selected);
        return ShowTable(counter, sortbyField, sortDirection, selected,userid,Filtervalue); 
    }
    private static LightProductTemplateListV ShowTable(Integer counter, String sortbyField, String sortDirection, String selected, String userid,String Filtervalue){ 
        LightProductTemplateListV retValue = new LightProductTemplateListV();
        try{
            String sessionselectedchk='';
            if((String)Cache.Session.get('sessionselectedPT') != null)  
                sessionselectedchk= Helper.getSessionValue('sessionselectedPT');  
            String whereCondition = ''; 
            if(sessionselectedchk == 'Active' || sessionselectedchk == ''){
                whereCondition = ' WHERE Active__c = True';
                
            }
            else if(sessionselectedchk == 'All'){
                whereCondition = ' WHERE (Active__c = True OR Active__c = false)';
            }
            else if(sessionselectedchk == 'Inactive'){
                whereCondition = ' WHERE Active__c = false';
            }
            if(Filtervalue!='All Realms' && Filtervalue!=null)
            {
                String setTids='(';
                if(Filtervalue.startsWith('a0M')){
                    
                    for(Tenant__c T:[SELECT id FROM Tenant__c WHERE Organization__c=:Filtervalue])
                    {
                       setTids =setTids +'\''+T.id+'\',';                        
                    }
                    setTids =setTids+')';
                    if(setTids != '(')
                    {
                        system.debug('##'+setTids);
                     String FinalsetTids = setTids.replace(',)', ')');
                        system.debug('#FinalsetTids#'+FinalsetTids);
                     whereCondition = whereCondition + ' AND Tenant__c IN'+FinalsetTids;
                        
                    }
                }else{
                whereCondition = whereCondition + ' AND Tenant__c=\''+Filtervalue+'\'';
                }
             }
            size = [SELECT count() from UI_List_Size__c WHERE User__c =: UserInfo.getUserId() AND Name =: 'TemplateLists' AND Grid_Name__c =: 'TemplateLists']; 
            
            if(size > 0){
                retValue.list_size = Integer.valueOf([SELECT List_Size__c from UI_List_Size__c WHERE User__c =: UserInfo.getUserId() AND Name =: 'TemplateLists' AND Grid_Name__c =: 'TemplateLists'][0].List_Size__c);
            }
            else {
                retValue.list_size = 25;
            }
            
            
            String query = 'SELECT Id, Name, ProductType__c,Transaction_Count__c,Attributes_Count__c, TemplateName__c, Active__c,IsPrimary__c,CreatedDate,LastModifiedDate,Created_ByName__r.Last_Name__c,Created_ByName__r.First_Name__c,CreatedBy.name,CreatedById FROM ProductTemplate__c';
            //query = query + ' WHERE CreatedById = ' + UserInfo.getUserId();
            
            List<User_Management__c> lstName= [Select Id,Tenant__c,Tenant__r.Tenant_Short_Name__c,Tenant__r.Tenant_Logo_Url__c,First_Name__c,Last_Name__c,Profiles__c from User_Management__c where Id =: userid LIMIT 1];
            
            if(lstName[0].Profiles__c== System.label.ACP_ProfileID)
            {
                query = query + whereCondition + ' ORDER BY ' + sortbyField + ' ' + sortDirection + ' LIMIT ' + retValue.list_size + ' offset ' + counter;
                
            }else{
                //query = query + whereCondition + ' AND Created_ByName__c=\''+lstName[0].Id+'\' ORDER BY ' + sortbyField + ' ' + sortDirection + ' LIMIT ' + retValue.list_size + ' offset ' + counter;
                query = query + whereCondition + ' AND Tenant__c=\''+lstName[0].Tenant__c+'\' ORDER BY ' + sortbyField + ' ' + sortDirection + ' LIMIT ' + retValue.list_size + ' offset ' + counter;
                
            }
            system.debug('=========query==========='+query);
            retValue.results = Database.query(query);
            system.debug('=========retValue.results==========='+retValue.results);
            List <ProductTemplate__c> proTemplateList = new List <ProductTemplate__c>();
            List <User> userList = [SELECT Id, Name FROM User];
            for(ProductTemplate__c proTemplate : retValue.results) {
                proTemplate.Transaction_Count__c=[SELECT count() FROM Transaction__c where Product_Template__c=:proTemplate.Id];
                proTemplate.Attributes_Count__c =[SELECT count() FROM Product_Template_Object__c where Product_Template__c=:proTemplate.Id];
                proTemplateList.add(proTemplate);
            }
            retValue.results = proTemplateList;
            system.debug('=========retValue.results==========='+retValue.results);
            retValue.total_size = proTemplateList.size();
            retValue.counter = counter; 
            retValue.sortbyField = sortbyField; 
            retValue.sortDirection = sortDirection; 
            retValue.total_page = getTotalPages(retValue.list_size, retValue.total_size);
            retValue.selectedItem = (sessionselectedchk == '') ? 'Active': sessionselectedchk;
        } catch(Exception ex){
            //Logger.LogException('Error - LightProductTemplate - ShowTable()',ex.getMessage());
        }
        
        return retValue;    
    }
    
    @AuraEnabled
    public static LightProductTemplateListV getProductTemplateList(Integer counter, String sortbyField, String sortDirection, String selected, String userid,String Filtervalue){           
        LightProductTemplateListV retValue = new LightProductTemplateListV(); 
        try{  
            if((String)Cache.Session.get('sortbyFieldSOL') != null)  
                sortbyField = Helper.getSessionValue('sortbyFieldSOL');  
            
            if((String)Cache.Session.get('sortDirectionSOL') != null)  
                sortDirection = Helper.getSessionValue('sortDirectionSOL'); 
            retValue = ShowTable(counter, sortbyField, sortDirection, selected, userid, Filtervalue);  
        } catch(Exception ex){
            
        }
        return retValue;
    } 
    
    @AuraEnabled
    public static LightProductTemplateListV getBeginning(Integer counter, String sortbyField, String sortDirection, String selected, String userid,String Filtervalue) { 
        return ShowTable(counter, sortbyField, sortDirection, selected, userid, Filtervalue); 
    }
    
    @AuraEnabled
    public static LightProductTemplateListV Views(Integer counter, String sortbyField, String sortDirection, String selected, String userid,String Filtervalue) { 
        Helper.setSessionValue('sessionselectedPT', selected);
        return ShowTable(counter, sortbyField, sortDirection, selected, userid, Filtervalue); 
    }
    
    @AuraEnabled
    public static LightProductTemplateListV getPrevious(Integer counter, String sortbyField, String sortDirection, String selected, String userid,String Filtervalue) {
        return ShowTable(counter, sortbyField, sortDirection, selected, userid, Filtervalue); 
    }
    
    @AuraEnabled
    public static LightProductTemplateListV getNext(Integer counter, String sortbyField, String sortDirection, String selected, String userid,String Filtervalue) {  
        return ShowTable(counter, sortbyField, sortDirection, selected, userid, Filtervalue); 
    }
    
    @AuraEnabled
    public static LightProductTemplateListV getEnd(Integer counter, String sortbyField, String sortDirection, String selected, String userid,String Filtervalue) { 
        return ShowTable(counter, sortbyField, sortDirection, selected, userid, Filtervalue); 
    } 
    
    private static Integer getTotalPages(Integer list_size, Integer total_size) { 
        if (math.mod(total_size, list_size) > 0) {
            return total_size/list_size + 1;
        }
        else {
            return (total_size/list_size);
        }
    }  
    
    @AuraEnabled
    public static LightProductTemplateListV changelist_size(Integer counter, String sortbyField, String sortDirection, Integer list_size, String selected, String userid,String Filtervalue) { 
        LightProductTemplateListV retValue = new LightProductTemplateListV(); 
        try{
            size = [SELECT count() from UI_List_Size__c WHERE User__c =: UserInfo.getUserId() AND Name =: 'TemplateLists' AND Grid_Name__c =: 'TemplateLists'];
            if(size > 0){  
                UI_List_Size__c listSize = new UI_List_Size__c(
                    Id = [SELECT Id from UI_List_Size__c WHERE User__c =: UserInfo.getUserId() AND Name =: 'TemplateLists' AND Grid_Name__c =: 'TemplateLists'][0].Id,
                    List_Size__c = list_size  
                );
                update listSize;
            }
            else {
                UI_List_Size__c listSize = new UI_List_Size__c(
                    List_Size__c = list_size,
                    Name = 'TemplateLists',
                    Grid_Name__c = 'TemplateLists',
                    User__c = UserInfo.getUserId()
                );
                insert listSize;
            }  
            
            retValue = ShowTable(counter, sortbyField, sortDirection, selected, userid, Filtervalue);
            
        } catch(Exception ex){
            //Logger.LogException(
             //   'Error - LightProductTemplate - changelist_size()', 
             //   ex.getMessage()
//);
        }
        return retValue;
    }
    
    @AuraEnabled
    public static LightProductTemplateListV SortTable(Integer counter, String sortbyField, String sortDirection, String selected, String userid,String Filtervalue) { 
        LightProductTemplateListV retValue = new LightProductTemplateListV(); 
        try{
            if(sortbyField == '') {
                sortbyField = 'LastModifiedDate';
                sortDirection = 'DESC';
            } else {
                if(sortDirection.equals('ASC')){
                    sortDirection = 'DESC';
                } else {
                    sortDirection = 'ASC';
                }
            }
            Helper.setSessionValue('sortbyFieldSOL', sortbyField);
            Helper.setSessionValue('sortDirectionSOL', sortDirection); 
            retValue = ShowTable(counter, sortbyField, sortDirection, selected, userid, Filtervalue); 
        } catch (Exception ex){ 
           // Logger.LogException(
           //     'Error - LightProductTemplate - SortTable()', 
           //     ex.getMessage()
           // );
        }
        return retValue;
    } 
    
    @AuraEnabled
    public static LightProductTemplateListV changePage(Integer counter, String sortbyField, String sortDirection, String selected, String userid,String Filtervalue) {
        return ShowTable(counter, sortbyField, sortDirection, selected, userid, Filtervalue); 
    }
    @AuraEnabled
    public static List<TemplateField> getSourceFieldsList(string productType, string marketType){
        List<TemplateField> retValue = new List<TemplateField>();
        try{
            String whereCondition = ''; 
            if(marketType == 'false'){
                whereCondition = ' WHERE ProductType__c = \'' + productType + '\' AND IsSecondary__c = true ORDER BY Name';
            }
            else if(marketType == 'true'){
                whereCondition = ' WHERE ProductType__c = \'' + productType + '\' AND IsPrimary__c = true ORDER BY Name';
            }
            
            String query = 'SELECT Id, Name,Mandatory__c, Attribute_Size__c, ProductType__c, Attribute_Type__c, IsActive__c, IsPrimary__c, IsSecondary__c, CreatedDate FROM Attributes__c';
            query = query + whereCondition;
            List<Attributes__c> attributesList = Database.query(query);            
            
            for(Attributes__c attr : attributesList)
            {
                String value = attr.Name.replace('~' ,' ');
                value = value.replace('`' ,' ');
                value = value.replace('@' ,' ');
                value = value.replace('#' ,' ');
                value = value.replace('$' ,' ');
                value = value.replace('%' ,' ');
                value = value.replace('^' ,' ');
                value = value.replace('&' ,' ');
                value = value.replace('*' ,' ');
                value = value.replace('(' ,' ');
                value = value.replace(')' ,' ');
                value = value.replace('-' ,' ');
                value = value.replace('_' ,' ');
                value = value.replace('=' ,' ');
                value = value.replace('+' ,' ');
                value = value.replace('{' ,' ');
                value = value.replace('}' ,' ');
                value = value.replace('[' ,' ');
                value = value.replace(']' ,' ');
                value = value.replace('|' ,' ');
                value = value.replace('/' ,' ');
                value = value.replace('\\' ,' ');
                value = value.replace('?' ,' ');
                value = value.replace('>' ,' ');
                value = value.replace('<' ,' ');
                value = value.replace('.' ,' ');
                value = value.replace('    ' ,' ');
                value = value.replace('   ' ,' ');
                value = value.replace('  ' ,' ');
                
                 retValue.add(new TemplateField(
                   value.trim(), 
                   attr.Name.trim(),
                   attr.Id,
                   attr.Attribute_Type__c, 
                   String.valueOf(attr.Attribute_Size__c),
                   attr.Mandatory__c,
                   attr.CreatedByName__c,
                   attr.LastModifiedByName__c
                   
               ));
            }
           
        } catch(Exception ex){
            //Logger.LogException(
            //    'Error - LightProductTemplate - getSourceFieldsList()', 
           //     ex.getMessage()
           // );
        }
        return retValue;
    }
    
    @AuraEnabled
    public static ProductTemplateV getProductTemplate(string templateId, string productType, string marketType, string templateName){
        ProductTemplateV retValue = new ProductTemplateV();
        try{
            if(string.isBlank(templateId) || string.isEmpty(templateId)){
                retValue.ProductType = productType; 
                retValue.IsPrimary = Boolean.valueOf(marketType);
                retValue.TemplateName = templateName;
                retValue.SourceFields = getSourceFieldsList(productType,marketType);
                retValue.SelectedFields = new List<String>();
            }
        } catch(Exception ex){
            
        }
        return retValue;
    }
    
    private static TemplateField getTemplateField(List<TemplateField> sourceFields, String value) {
        TemplateField retValue = new TemplateField('', '');
        try{
            for(Integer j = 0; j < sourceFields.size(); j++){
                if(sourceFields.get(j).value == value){
                    retValue =  (TemplateField)sourceFields.get(j); 
                    break;
                }
            }
        } catch(Exception ex){
          //  throw new AppException('getTemplateField() - ' + ex.getMessage());
        }
        return retValue;
    }
    @AuraEnabled
    public static String saveProductTemplate(string template){
        String response = '';
        try {  
            
            ProductTemplateV templateObject = (ProductTemplateV)System.JSON.deserializeStrict(
                template, 
                ProductTemplateV.Class
            );
            
            Integer count = [SELECT count() from ProductTemplate__c WHERE TemplateName__c = :templateObject.TemplateName]; 
            if(count > 0){
                response = 'Already Used';
            }
            else {               
                ProductTemplate__c result = insertProductTemplate(template);
                response = result.Id;
            }  
        } catch(Exception ex){
            System.debug('LightProductTemplate - saveProductTemplate() : ' + ex.getMessage());
            response = '';
        }
        return response; 
    }
    private static ProductTemplate__c insertProductTemplate(String template){
        ProductTemplate__c retValue = new ProductTemplate__c();
        try {
            ProductTemplateV templateObject = (ProductTemplateV)System.JSON.deserializeStrict(
                template, 
                ProductTemplateV.Class
            );
            
            retValue = new ProductTemplate__c(
                ProductType__c = templateObject.ProductType,
                TemplateName__c = templateObject.TemplateName,
                Active__c = true,
                IsPrimary__c = templateObject.IsPrimary
              
            );
            insert retValue;       
        } catch(Exception ex){
           // throw new AppException('insertProductTemplate() - ' + ex.getMessage());
        }
        return retValue;
    }
    @AuraEnabled
    public static String CheckProductTemplate(String txtTemplateName) { 
        String response = '';
        try {  
            Integer count = [SELECT count() from ProductTemplate__c WHERE TemplateName__c = :txtTemplateName]; 
            if(count > 0){
                response = 'Already Used';
            }
            else { 
                response = '';
            }  
        } catch(Exception ex){
            System.debug('LightProductTemplate - CheckProductTemplate() : ' + ex.getMessage());
            
            response = '';
        }
        return response; 
    } 
  
}