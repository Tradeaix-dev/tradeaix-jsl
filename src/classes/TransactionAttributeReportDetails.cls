public class TransactionAttributeReportDetails {
  public map<string, list<String>> getDynamicAttributesInfo(){
        
        map<string, list<String>> mapAttList = new map<string, list<String>>();
        string a = '';
        for(Transaction__c  T: [SELECT Id, TransactionRefNumber__c,Product_Template__r.TemplateName__c from Transaction__c limit 20]){
                List<String> ls = new List<String>();
                String atts = '';
                for(Transaction_Attributes__c TA: [SELECT Id, Attribute_Name__c,Attribute_Value__c,Transaction__r.TransactionRefNumber__c from Transaction_Attributes__c WHERE Transaction__c =: T.Id]){
                    //ls.add(TA.Attribute_Name__c+':'+TA.Attribute_Value__c);
                    if(atts == ''){
                        atts+= '10'+'|';
                        atts+= TA.Transaction__r.TransactionRefNumber__c+'|';
                    }
                    atts+=TA.Attribute_Name__c+':'+TA.Attribute_Value__c+'|';
                    a = TA.Transaction__r.TransactionRefNumber__c;
                }
                ls.add(atts);
               
                if(!mapAttList.containsKey(T.TransactionRefNumber__c)){
                    mapAttList.put(a, ls );
                }
        }      
        return mapAttList;
    }
}