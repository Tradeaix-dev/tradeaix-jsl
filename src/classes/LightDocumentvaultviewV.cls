public class LightDocumentvaultviewV {
@AuraEnabled
    public List <ContentVersion> results = new List <ContentVersion>();
@AuraEnabled
    public List <Transaction__c> offinfo = new List <Transaction__c>();   
@AuraEnabled
    public List <Quotes__c> syninfo = new List <Quotes__c>();       
    @AuraEnabled
    public Integer counter = 0; 
    @AuraEnabled
    public String selectedItem = ''; 
    @AuraEnabled
    public String showpage = '';
    @AuraEnabled
    public String sortbyField = '';
    @AuraEnabled
    public String sortDirection = '';
    @AuraEnabled
    public Integer total_page = 0; 
    @AuraEnabled
    public Integer list_size = 0;
    @AuraEnabled
    public Integer total_size = 0;  
	@AuraEnabled
    public String FileUploadUrl='';
    @AuraEnabled
    public String FileshareFlag='';
     @AuraEnabled
    public String Filefoldersname='';
    @AuraEnabled
    public Boolean isFilterA = false;
    @AuraEnabled
    public UI_Filter__c filter = new UI_Filter__c();
    @AuraEnabled
    public List <Document_Vault_Folder__c> folderresult = new List <Document_Vault_Folder__c>();
    @AuraEnabled
    public String selectedfoldername='';  
    //@AuraEnabled
    //public List <User_Management__c> userlist = new List <User_Management__c>();
    @AuraEnabled
    public List <File_Sharing__c> userlist = new List <File_Sharing__c>();
}