public without sharing  class TaskupdateEmailclass {
    public Id activelogId {get;set;} 
    public class TaskDetails { 
        public String userId {get;set;}
        public String tasktype {get;set;}
        public String tasksubject {get;set;}
        public String duedate {get;set;}  
        public String priority{get;set;}
        public String logo{get;set;}
        public String logotitle{get;set;} 
        public String Transref{get;set;} 
        public String status{get;set;} 
        public String comments{get;set;}  
        public String transId{get;set;} 
    }
    public TaskDetails email {get;set;}  
    public TaskDetails getdetails()
    {
        email = new TaskDetails();
        try{
            Activity_Log__c AL = [SELECT Id, Action_By__c, DueDate__c, Operation_Log__c, Priority__c, Status__c, TaskName__c, TaskSubject__c, TaskType__c, TransRefNumber__c from Activity_Log__c WHERE Id =: activelogId];
        	email.transId = [SELECT Id from Transaction__c WHERE TransactionRefNumber__c =: AL.TransRefNumber__c].Id;
            email.tasktype = AL.TaskType__c;
            email.tasksubject = AL.TaskSubject__c;
            email.duedate = String.valueOf(AL.DueDate__c);
            email.priority = AL.Priority__c;
            email.status = AL.Status__c;
            email.comments = AL.Operation_Log__c;
            email.logo = system.label.TradeAix_Logo;
        }catch(exception ex){
            system.debug('//****// '+ex.getMessage());           
        }
        return email;
    }
}