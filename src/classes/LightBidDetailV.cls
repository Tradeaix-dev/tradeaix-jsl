public class LightBidDetailV {
        @AuraEnabled
        public Quotes__c Syndication{get;set;}
        @AuraEnabled
        public Transaction__c Transactions{get;set;}
       
        @AuraEnabled
        public String offerId {get;set;}
        @AuraEnabled
        public String attrId {get;set;}
        @AuraEnabled
        public String syndId {get;set;}
        @AuraEnabled
        public String synname {get;set;}
        @AuraEnabled
        public Boolean dueFlag {get;set;}
        @AuraEnabled
        public Boolean flag {get;set;}
        @AuraEnabled
        public Boolean newFlag {get;set;}
        @AuraEnabled
        public string offerName {get;set;}
        @AuraEnabled
        public Boolean IsPrimary {get;set;}
        @AuraEnabled
        public string offerType {get;set;}
        @AuraEnabled
        public string offerStatus {get;set;}  
    @AuraEnabled
        public string IssuingBanks {get;set;}  
        
        @AuraEnabled
        public List <Attachment> attachment {get;set;}
        @AuraEnabled
        public List <ContentVersion> content1 {get;set;}
       
        @AuraEnabled
        public List <Activity_Log__c> ActivityLog1 {get;set;}
        @AuraEnabled
        public Boolean isSuperUser =false;
        @AuraEnabled
        public Boolean canShowBidAction = false;
        @AuraEnabled
        public String FileUploadUrl='';
        @AuraEnabled
        public String finalizedOfferId = '';
        @AuraEnabled
        public User offerUser {get;set;}
        @AuraEnabled
    public User loginUser {get;set;}
    @AuraEnabled
    public String Logo {get;set;}  
    @AuraEnabled 
    public String LogoURL = null; 
    @AuraEnabled 
    public String CurrencyCode {get;set;} 
    @AuraEnabled 
    public String CurrencyName {get;set;} 
    @AuraEnabled 
    public Decimal TAmount {get;set;} 
    @AuraEnabled 
    public List<Activity_Log__c> Activity_Log = new List<Activity_Log__c> ();
    @AuraEnabled 
    public List <ContentVersion> ContentVer = New List<ContentVersion>();  
    @AuraEnabled 
    public List <Note> Internalmemos = New List<Note>();  
    

    }