public class LightTemplateAttributesV { 
    @AuraEnabled
    public List <Attributes__c> results = new List <Attributes__c>();
    @AuraEnabled
    public List <Activity_Log__c> Alogs = new List <Activity_Log__c>();
    @AuraEnabled
    public Integer counter = 0; 
    @AuraEnabled
    public String selectedItem = ''; 
    @AuraEnabled
    public String showpage = '';
    @AuraEnabled
    public String sortbyField = '';
    @AuraEnabled
    public String sortDirection = '';
    @AuraEnabled
    public Integer total_page = 0; 
    @AuraEnabled
    public Integer list_size = 0;
    @AuraEnabled
    public Integer total_size = 0;      
}