public class Transactionviewctrlv1 {
    @AuraEnabled
    public Transaction__c Trans = new Transaction__c(); 
    @AuraEnabled
    public List<Transaction_Attributes__c> TransAtt { get;set; }
    @AuraEnabled
    public List<Transaction_Attributes__c> TransAttAmend { get;set; }
    @AuraEnabled
    public List<Transaction_Attributes__c> TransAttAmendAccepted { get;set; }
    @AuraEnabled
    public List<Transaction_Attributes__c> TransAttOldValues { get;set; }
    @AuraEnabled
    public List<Amend_Attributes__c> TransAttAmendgroup { get;set; }    
    @AuraEnabled 
    public List<Published_Tenant__c> CounterParties = new List<Published_Tenant__c> ();
    @AuraEnabled 
    public List<Published_Tenant__c> CounterParties1 = new List<Published_Tenant__c> ();
    @AuraEnabled 
    public List<Request_Information_Quote__c> RequestInformation = new List<Request_Information_Quote__c> ();
    @AuraEnabled 
    public List<Quotes__c> Quotes = new List<Quotes__c> ();
    @AuraEnabled 
    public List<RequestedNote__c> RequestedNote= new List<RequestedNote__c> ();    
    @AuraEnabled
    public String LogeduserProfileId = '';
    @AuraEnabled
    public String FileUploadUrl = '';
    @AuraEnabled
    public String FileUploadUrl1 = '';
    @AuraEnabled
    public String FileUploadUrl2 = '';
    @AuraEnabled 
    public List<Activity_Log__c> Activity_Log = new List<Activity_Log__c> ();
    @AuraEnabled 
    public List<Note> Internalmemos = new List<Note> ();
    @AuraEnabled 
    public List <ContentVersion> ContentVer = New List<ContentVersion>();  
    @AuraEnabled
    public Boolean isbidacceptclose = false;
    @AuraEnabled 
    public List<Published_Tenant_User__c> CounterPartiesUsers = new List<Published_Tenant_User__c> ();
    @AuraEnabled 
    public List<User_Management__c> userslist = new List<User_Management__c>();
    @AuraEnabled 
    public List<Organization__c> Org = new List<Organization__c>();
    @AuraEnabled 
    public List<CPSelection__c> CPSelection = new List<CPSelection__c>();
    @AuraEnabled 
    public List<User_Management__c> userslist1 = new List<User_Management__c>();
    @AuraEnabled 
    public List<Task> Tasks = new List<Task>();
    @AuraEnabled 
    public List<Task> ClosedTasks = new List<Task>();
    @AuraEnabled
    public Boolean isTaskclosedstatus = false;
    @AuraEnabled 
    public List<Activity_Log__c> TaskActivity_Log = new List<Activity_Log__c> ();
    @AuraEnabled
    public Task taskdetails {get;set;} 
    @AuraEnabled
    public Boolean isTaskclose = false;
    @AuraEnabled
    public Boolean isAmend = false;
    @AuraEnabled
    public Boolean isAmendAccept = false;
    @AuraEnabled 
    public List<String> CPuserlist = new List<String> ();
    @AuraEnabled 
    public List<String> idString = new List<String>();
    
}