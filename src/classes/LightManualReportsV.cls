public class LightManualReportsV {
    @AuraEnabled
    public List<Report__c> reportlists {get;set;}
    @AuraEnabled
    public List<Attributes__c> AttributesList {get;set;}
    @AuraEnabled
    public List<ProductTemplate__c> getTemplates {get;set;}
    @AuraEnabled
    public List<Tenant__c> getcp {get;set;}
    @AuraEnabled
    public List<Quotes__c> getQuotes{get;set;}
}