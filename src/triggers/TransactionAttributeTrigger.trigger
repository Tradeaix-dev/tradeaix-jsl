trigger TransactionAttributeTrigger on Transaction_Attributes__c (before insert, after update) {
    try{
        for (Transaction_Attributes__c  AL: Trigger.new) {
            if(Trigger.isBefore && Trigger.isInsert){
                TimeZone tz = UserInfo.getTimeZone();
                AL.CreatedDatePDF__c = System.Now().format('dd/MM/YYYY HH:mm:ss', tz.getID());
            }
        }
        If(Trigger.isAfter && Trigger.isUpdate){
            for(Transaction_Attributes__c t : Trigger.New){ 
                Transaction_Attributes__c oldTrans = Trigger.oldMap.get(t.ID);
                //Changed Status from Transaction Created to Transaction Approved.
                Helper.ActivityLogInsertCallForRecord(t.LastModifiedBy__c,t.Transaction__c,'Changed '+t.Attribute_Name__c+' from '+ oldTrans.Attribute_Value__c+' to '+t.Attribute_Value__c,'Attributes Successfully Updated.',false);
            }
            TransactionTriggerHandler.SentMailAttributeUpdate(Trigger.new,Trigger.oldMap);
        }
        
    }catch(exception ex){
        system.debug('==TransactionAttributeTrigger=='+ex.getMessage());
    }

}