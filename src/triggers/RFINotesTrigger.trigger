trigger RFINotesTrigger on RequestedNote__c (after insert) {
    
    EmailManager em = new EmailManager();  
    for(RequestedNote__c  PT : Trigger.New){    
        try{
            String toEmail = [SELECT User_Email__c from User_Management__c WHERE Id=:PT.Requested_To__c limit 1].User_Email__c;
            String ccEmail = [SELECT User_Email__c from User_Management__c WHERE Id=:PT.CreatedBy__c limit 1].User_Email__c;
            List<String> CcAddresses = new List<String>();
            CcAddresses.add(ccEmail);
            
            String temaplateId = [SELECT Id from EmailTemplate WHERE Name = 'RFINotesEmailTemp' LIMIT 1].Id;
            em.sendMailWithTemplate(toEmail , CcAddresses , temaplateId , userinfo.getuserid(), string.valueOf(PT.Id));
            //String CreatedID=[SELECT id FROM User_Management__c WHERE Tenant__c=:PT.Tenant__c].Id;
            if(PT.WTQ__c!=null){
                Helper.NotificationInsertCall(PT.CreatedBy__c,PT.Requested_To__c, 'Request For Information ('+PT.WTQ__c+')', PT.Body__c,PT.RFIReference__c,PT.Transaction__c);
            }
            else{
                Helper.NotificationInsertCall(PT.CreatedBy__c,PT.Requested_To__c, 'Request For Information', PT.Body__c,PT.RFIReference__c,PT.Transaction__c);
                
            }
        }catch(exception ex){
            system.debug('==RFINotesTrigger Trigger Exception=='+ex.getMessage());
        }
    }
}