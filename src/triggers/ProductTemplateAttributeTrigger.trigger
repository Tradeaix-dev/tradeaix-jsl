trigger ProductTemplateAttributeTrigger on Product_Template_Object__c (after insert) {
    EmailManager em = new EmailManager();  
    for(Product_Template_Object__c PTO : Trigger.New){    
        try{
            ProductTemplate__c PT = [SELECT Id, Name,Tenant__c,Isclonechk__c from ProductTemplate__c WHERE Id =: PTO.Product_Template__c];
            String toEmail = [SELECT User_Email__c from User_Management__c WHERE Tenant__c=: PT.Tenant__c limit 1].User_Email__c;
            List<String> CcAddresses = new List<String>();
            
            if (Trigger.isInsert) {
                  if(PT.Isclonechk__c == true){
                      String temaplateId = [SELECT Id from EmailTemplate WHERE Name = 'TemplateCloneEmailTemplate' LIMIT 1].Id;
                      em.sendMailWithTemplate(toEmail , CcAddresses , temaplateId , userinfo.getuserid(), string.valueOf(PTO.Id));
                  }else{
                      String temaplateId = [SELECT Id from EmailTemplate WHERE Name = 'TemplateCreationEmailTemp' LIMIT 1].Id;
                      em.sendMailWithTemplate(toEmail , CcAddresses , temaplateId , userinfo.getuserid(), string.valueOf(PTO.Id));
                  }
                  
              }
        }catch(exception ex){
            system.debug('==ProductTemplate__c Trigger Exception=='+ex.getMessage());
        }
    }
}