<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <rules>
        <fullName>Sent Notification to Disagree</fullName>
        <active>false</active>
        <formula>AND(Do_Not_Agree__c==TRUE,ISCHANGED(Do_Not_Agree__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
